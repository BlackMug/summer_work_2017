// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <string>
#include <iostream>
#include <sstream>
#include <stdarg.h>
#include <fstream>
#include <regex>
#include "Frame.h"

// This example works only for YV12(YUV420)

struct input_settings
{
    std::string in_filename = "";
    size_t width = 0;
    size_t height = 0;
    bool just_print_help = false;
};

void print_help(char* app_name = "<app_name>") 
{
    std::cout << "Usage: " << app_name << " -i <file_name> -w <frame_width> -h <frame_height>\n" << std::endl;
}

void parse_args(int argc, char** argv, input_settings *s) 
{
    std::stringstream ss;
    static const int32_t skip_arg_num = 1; // Skip execution file path
    for (int32_t argc_it = skip_arg_num; argc_it < argc; argc_it++)
    {
        ss << argv[argc_it] << " ";
    }

    s->just_print_help = true;
    std::string arg;
    while (ss.rdbuf()->in_avail())
    {
        s->just_print_help = false;

        ss >> arg;
        if (arg == "-i")
        {
            ss >> s->in_filename;
        }
        else if (arg == "-w")
        {
            ss >> s->width;
        }
        else if (arg == "-h")
        {
            ss >> s->height;
        }
        else if (arg == "--help")
        {
            s->just_print_help = true;
        }
        else
        {
            std::cout << "Unknown argument: " <<  arg.c_str() << std::endl;
        }
    }
}

std::string get_out_filename(const std::string in_filename)
{
    const static std::string file_raw_ext = ".raw";
    const static std::string file_out_suffix = "_out";
    std::string out_filename(in_filename);

    if (size_t from = out_filename.find(file_raw_ext))
    {
        out_filename.replace(from, file_raw_ext.size(), file_out_suffix + file_raw_ext);
    }
    else
    {
        out_filename += file_out_suffix + file_raw_ext;
    }

    return out_filename;
}

// Return vector of YUV values from file
const std::vector<uint8_t> get_frame(std::ifstream &in_file, const size_t frame_size)
{
    std::vector<uint8_t> ret_buffer(frame_size);
    in_file.read((char*)ret_buffer.data(), frame_size);
    return ret_buffer;
}

// Test function for check either valid data 
//  was readed from file
int32_t print_one_frame(const input_settings s)
{
    int32_t err = 0;

    try
    {
        try
        {
            std::ifstream in_file(s.in_filename, std::ios::binary);

            std::string out_filename = get_out_filename(s.in_filename);
            std::ofstream out_file(out_filename, std::ios::binary);

            Frame frame(s.width, s.height);
            const auto frame_size = frame.get_frame_size();

            in_file.seekg(0, std::ios::beg);
			//for (int i = 0; i < 100; i++)
			//{
				frame.fill_data(get_frame(in_file, frame_size));
				frame.convert_YV12_to_RGB();
				out_file.write(frame.get_data_to_write(), frame.get_frame_size());
			//}
            out_file.close();
        }
        catch (std::ifstream::failure &e)
        {
            std::cout << "Error while opening file: " << e.what() << std::endl;
            err = 2;
        }
    }
    catch (const std::exception& e)
    {
        std::cout << "Error while printing frame: " << e.what() << std::endl;
        err = 1;
    }

    return err;
}

int main(int argc, char** argv)
{
    input_settings set;
    FILE *input_file = NULL;
    
    parse_args(argc, argv, &set);
    
    if (set.just_print_help)
    {
        print_help(argv[0]);
        exit(0);
    }

    if (set.in_filename == "")
    {
        std::cout << "Error: no input file specified" << std::endl;
        print_help(argv[0]);
        exit(0);
    }

    int8_t ret = print_one_frame(set);
	return ret;
}

