// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#ifndef COMMON_H
#define COMMON_H

const static constexpr uint32_t bits_in_byte = 8;
const static constexpr uint32_t bits_per_pixel_420 = 12;
const static constexpr uint32_t bits_per_pixel_444 = 24;

#endif
