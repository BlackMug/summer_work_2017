// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "Frame.h"
#include <iostream>

template <typename T>
static const std::tuple<T, T, T> Pixel::convert_YUV_to_RGB(T Y, T U, T V)
{
    T     C = Y - 16
        , D = U - 128
        , E = V - 128;
    return std::make_tuple(
        std::clamp((298 * C + 409 * E + 128) >> 8, 0, 255)          // R
        , std::clamp((298 * C - 100 * D - 208 * E + 128) >> 8, 0, 255)  // G
        , std::clamp((298 * C + 516 * D + 128) >> 8, 0, 255)          // B
    );
}

template <typename T>
static const std::tuple<T, T, T> Pixel::convert_RGB_to_YUV(T R, T G, T B)
{
    return std::make_tuple(
        static_cast<T>(0.299*R + 0.587*G + 0.114*B)   // Y'
        , static_cast<T>(-0.147*G - 0.289*G + 0.436*B)  // U
        , static_cast<T>(0.615*R - 0.515*G + 0.100*B)   // V
    );
}

Frame::Frame(const size_t width, const size_t height)
    : m_width(width)
    , m_height(height)
{
}

Frame::Frame(const size_t width, const size_t height
    , const std::vector<uint8_t> in_data)
    : m_width(width)
    , m_height(height)
    , m_data(in_data)
{
}

Frame::~Frame()
{
}

void Frame::fill_data(const std::vector<uint8_t> in_data)
{
    m_data = std::vector<uint8_t>(in_data);
}

const char * Frame::get_data_to_write()
{
    return reinterpret_cast<const char*> (m_data.data());
}

void Frame::set_data(const std::vector<uint8_t> data)
{
    m_data.clear();
    m_data = data;
    m_frame_size.reset();
}

// return size of frame in bytes
const size_t Frame::get_frame_size()
{
    if (!m_frame_size.has_value())
        m_frame_size = get_frame_size(m_width, m_height);
    return m_frame_size.value();
}

// Planar (420):
// YY   (field)
// YY
// V(field) U(field)
//
// Planar (444):
// YY   (field)
// YY
// VV   (field)
// VV
// UU   (field)
// UU
//std::vector<uint8_t> Frame::planar420_to_planar444(const std::vector<uint8_t> &in_data)
//{
//  std::vector<uint8_t> ret_data;
//
//  const uint32_t pixels_in_frame = m_width * m_height;
//  constexpr auto color_height_ratio = 2;                              // The ratio of Y height to U or V height
//  constexpr auto color_width_ratio = 2;                               // The ratio of Y width to U or V width
//  constexpr auto color_fields_ratio                                   // The ratio of Y field to U or V field
//      = color_height_ratio*color_width_ratio;
//  
//  const size_t Y_width = m_width;
//  const size_t V_width = Y_width / color_width_ratio;
//  const size_t U_width = Y_width / color_width_ratio;
//
//  auto begin_ = cbegin(in_data);
//
//// planar422:
//// YY
//// YY
//// V
//// V
//// U
//// U
//  auto count = 0;
//  std::vector<uint8_t> data_422;
//  auto iteration = 1;                                                             // Start from second
//  for (auto
//        Y_iter = begin_
//      , V_iter = std::next(begin_, pixels_in_frame)
//      , U_iter = std::next(begin_, pixels_in_frame + pixels_in_frame/color_fields_ratio)
//      , Y_end = V_iter
//      ; Y_iter != Y_end
//      ; std::advance(Y_iter, Y_width)
//      , std::advance(V_iter, V_width * (!(iteration%color_width_ratio)))          // Every second iteration
//      , std::advance(U_iter, U_width * (!(iteration%color_width_ratio)))          // Every second iteration
//      , iteration++
//      )
//  {
//      std::copy(Y_iter, std::next(Y_iter, Y_width), std::back_inserter(data_422));
//      std::copy(V_iter, std::next(V_iter, V_width), std::back_inserter(data_422));
//      std::copy(U_iter, std::next(U_iter, U_width), std::back_inserter(data_422));
//      count+=3;
//  }
//  std::cout << "planar 420 -> planar 422. Operations count: " << count << std::endl;
//
//// planar442
//  count = 0;
//  const auto begin_422 = cbegin(data_422);
//  const auto VU_begin = std::next(begin_422, Y_width);
//
//  std::copy(begin_422, VU_begin, std::back_inserter(ret_data));
//
//  for ( auto
//        VU_iter = VU_begin
//      , end_state = cend(data_422)
//      ; VU_iter != end_state
//      ; std::advance(VU_iter, 1)
//      )
//  {
//      ret_data.emplace_back(*VU_iter);
//      ret_data.emplace_back(*VU_iter);
//      count+=2;
//  }
//  std::cout << "planar 422 -> planar 444. Operations count: " << count << std::endl;
//
//  return ret_data;
//}

// Planar (444):
// YY   (field)
// YY
// VV   (field)
// VV
// UU   (field)
// UU
//
// Packed:
// YVUYVU
// YVUYVU
//std::vector<uint8_t> Frame::planar444_to_packed444(const std::vector<uint8_t> &planar_data)
//{
//  std::vector<uint8_t> packed_data;
//  const uint32_t pixels_in_frame = m_width * m_height;
//
//  auto count = 0;
//  for (auto
//        Y_iter = cbegin(planar_data)
//      , V_iter = std::next(Y_iter, pixels_in_frame)
//      , U_iter = std::next(V_iter, pixels_in_frame)
//      , planar_end = cend(planar_data)
//      ; U_iter != planar_end
//      ; std::advance(Y_iter, 1)
//      , std::advance(V_iter, 1)
//      , std::advance(U_iter, 1)
//      )
//  {
//      packed_data.emplace_back(*Y_iter);
//      packed_data.emplace_back(*V_iter);
//      packed_data.emplace_back(*U_iter);
//      count+=3;
//  }
//  std::cout << "planar 444 -> packed 444. Operations count: " << count << std::endl;
//
//  return packed_data;
//}

void Frame::convert_YV12_to_RGB()
{
    //m_data = planar444_to_packed444(planar420_to_planar444(m_data));
    //m_data = planar420_to_packed444(m_data);
	set_data(planar420_to_packed444(m_data));

    auto count = 0;
    std::vector<uint8_t> RGB_data;
    for (auto iter = cbegin(m_data); iter != cend(m_data); std::advance(iter, 3))
    {
        uint8_t R, G, B;
        std::tie(R, G, B) = Pixel::convert_YUV_to_RGB<uint8_t>(*iter, *std::next(iter, 2), *std::next(iter, 1));
        RGB_data.emplace_back(R);
        RGB_data.emplace_back(G);
        RGB_data.emplace_back(B);
        count++;
    }
    std::cout << "YV12 -> RGB. Operations count: " << count << std::endl;

    set_data(RGB_data);
}

inline const size_t planar444_pos_to_planar420_pos(
    const size_t pk_pos,
    const size_t full_width, /*const size_t full_height,*/
    const uint32_t height_ratio, const uint32_t width_ratio)
{
    //auto short_width = full_width / width_ratio;
    //auto short_height = full_height / height_ratio;

    //auto pk_w = pk_pos % full_width;
    //auto pl_w = pk_w / width_ratio;

    //auto pk_h = pk_pos / full_height;
    //auto pl_h = pk_h / height_ratio;

    //auto pl_pos = pl_h * short_width + pl_w;

    //return pl_pos;

    return
        ((pk_pos / full_width) / height_ratio)
        * full_width / width_ratio // Short width
        + ((pk_pos % full_width) / width_ratio);
}


std::vector<uint8_t> Frame::planar420_to_packed444(const std::vector<uint8_t> &in_data)
{

    const auto pixels_in_frame = m_width * m_height;
    constexpr auto color_height_ratio = 2;                              // The ratio of Y height to U or V height
    constexpr auto color_width_ratio = 2;                               // The ratio of Y width to U or V width
    constexpr auto color_fields_ratio                                   // The ratio of Y field to U or V field
        = color_height_ratio*color_width_ratio;

    const size_t full_width = m_width;
    const size_t full_height = m_height;
    const size_t short_width = m_width / color_width_ratio;
    const size_t short_height = m_height / color_height_ratio;

    const static uint8_t planes_count = 3;
    std::vector<uint8_t> out_data(pixels_in_frame*planes_count);

    auto end444 =   end(out_data);
    auto Y444_begin = begin(out_data);
    auto V444_begin = std::next(Y444_begin, pixels_in_frame);
    auto U444_begin = std::next(V444_begin, pixels_in_frame);
    
    auto Y420_begin = begin(in_data);
    auto V420_begin = std::next(Y420_begin, pixels_in_frame);
    auto U420_begin = std::next(V420_begin, pixels_in_frame / color_fields_ratio);

	int32_t Y420_pos;
	int32_t V420_pos;
	int32_t U420_pos;

    for (auto
        Y444_pos = 0,
        V444_pos = 0,
        U444_pos = 0,
		pos = 0; // 444 pos

        U444_pos != pixels_in_frame;
		//pos != pixels_in_frame;

        Y444_pos++,
        V444_pos++,
        U444_pos++
		//pos++
        )
    {
		Y420_pos = Y444_pos; 
        V420_pos = planar444_pos_to_planar420_pos(V444_pos,
			full_width, color_height_ratio, color_height_ratio); 
		U420_pos = planar444_pos_to_planar420_pos(U444_pos,
			full_width, color_height_ratio, color_height_ratio);

        *std::next(Y444_begin, pos++) = *std::next(Y420_begin, Y420_pos);
        *std::next(Y444_begin, pos++) = *std::next(V420_begin, V420_pos);
        *std::next(Y444_begin, pos++) = *std::next(U420_begin, U420_pos);
    }

    bits_per_pixel = bits_per_pixel_444;
    return out_data;
}
