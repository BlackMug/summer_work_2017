// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#ifndef FRAME_CLASS
#define FRAME_CLASS
#include <vector>
#include <optional>
#include <tuple>
#include <algorithm>
#include "Common.h"


namespace Pixel
{
    template <typename T>
    static const std::tuple<T, T, T> convert_RGB_to_YUV(T R, T G, T B);

    template <typename T>
    static const std::tuple<T, T, T> convert_YUV_to_RGB(T Y, T U, T V);
};

class Frame
{
public:
    Frame(const size_t width, const size_t height);
    Frame(const size_t width, const size_t height, const std::vector<uint8_t> in_data);
    ~Frame();
    void fill_data(const std::vector<uint8_t> in_data);
    const char* get_data_to_write();
    void set_data(const std::vector<uint8_t> data);

    // Return frame size in bytes
    const size_t get_frame_size();
    inline const size_t get_frame_size(const size_t width, const size_t height)
    {
        return width * height * bits_per_pixel / bits_in_byte;
    }

    //std::vector<uint8_t> planar444_to_packed444(const std::vector<uint8_t> &in_data);
    //std::vector<uint8_t> planar420_to_planar444(const std::vector<uint8_t> &in_data);

    std::vector<uint8_t> planar420_to_packed444(const std::vector<uint8_t>& in_data);
    void convert_YV12_to_RGB();

private:
    std::vector<uint8_t> m_data;
    size_t m_width, m_height;
    std::optional<size_t> m_frame_size;
    uint32_t bits_per_pixel = bits_per_pixel_420; // default
};

#endif
